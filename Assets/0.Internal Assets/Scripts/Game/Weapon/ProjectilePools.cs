﻿using Common.Pool;
using GameSpace.Projectiles;
using System.Collections.Generic;
using UnityEngine;

namespace GameSpace.Weapons
{
    public class ProjectilePools : MonoBehaviour
    {
        public static ProjectilePools Instance;

        private Dictionary<WeaponController, IPool<Projectile>> projectiles;

        private void Awake()
        {
            Instance = this;
            projectiles = new Dictionary<WeaponController, IPool<Projectile>>();
        }

        public Projectile GetProjectile(WeaponController weapon)
        {
            return projectiles[weapon].GetItem();
        }

        public void PutBackProjectile(WeaponController weapon, Projectile projectile)
        {
            projectiles[weapon].PutBack(projectile);
        }

        public void CreateProjectilesPool(WeaponController weapon, Projectile projectile, int count)
        {
            if (!projectiles.ContainsKey(weapon))
            {
                projectiles.Add(weapon, PoolHelper.CreatePool<Projectile>(projectile, p => { p.Init(); }, transform, count));
            }
        }
    }
}