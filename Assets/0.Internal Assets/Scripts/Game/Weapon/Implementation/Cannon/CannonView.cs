﻿using UnityEngine;

namespace GameSpace.Weapons
{
    public class CannonView : WeaponView
    {
        [SerializeField] private WeaponRotor weaponRotor = null;
        [SerializeField] private Transform projectileStartPosition = null;

        private void Awake()
        {
            ProjectileStartPosition = projectileStartPosition;
            WeaponRotor = weaponRotor;
        }
    }
}