﻿using GameSpace.Projectiles;

namespace GameSpace.Weapons
{
    public class CannonController : WeaponController
    {
        public CannonController(WeaponView view, Projectile projectile, WeaponModel model) : base(view, projectile, model)
        {

        }
    }
}