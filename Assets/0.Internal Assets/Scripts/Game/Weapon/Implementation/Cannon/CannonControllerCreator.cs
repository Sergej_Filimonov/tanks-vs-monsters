﻿using GameSpace.Projectiles;
using UnityEngine;

namespace GameSpace.Weapons
{
    [CreateAssetMenu(fileName = "Cannon Controller Creator", menuName = "Game Elements/Weapons/Cannon Controller Creator")]
    public class CannonControllerCreator : WeaponControllerCreator
    {
        public override WeaponController CreateController(WeaponView view, WeaponModel data, Projectile projectile, Transform weaponPosition)
        {
            var cannonViewInstance = GameObject.Instantiate(view, weaponPosition);
            cannonViewInstance.transform.position = weaponPosition.position;
            cannonViewInstance.transform.rotation = weaponPosition.rotation;
            return new CannonController(cannonViewInstance, projectile, data);
        }
    }
}