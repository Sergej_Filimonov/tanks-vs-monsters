﻿using System;
using UnityEngine;

namespace GameSpace.Weapons
{
    public class WeaponRotor : MonoBehaviour
    {
        [SerializeField] private Transform frame = null;
        [SerializeField] private Transform barrel = null;
        [SerializeField] private Limit barrelAngleLimitX = new Limit();
        [SerializeField] private Limit barrelAngleLimitY = new Limit();

        private float angleX = 0;
        private float angleY = 0;
        private Vector3 barrelRotation = new Vector3();
        private Vector3 mainRotation = new Vector3();

        public void ApplyRotation(Transform source)
        {
            angleX = source.localEulerAngles.x;
            angleY = source.localEulerAngles.y > 180 ? source.localEulerAngles.y - 360.0f : source.localEulerAngles.y;
            
            angleX = Mathf.Clamp(angleX, barrelAngleLimitX.Min, barrelAngleLimitX.Max);
            angleY = Mathf.Clamp(angleY, barrelAngleLimitY.Min, barrelAngleLimitY.Max);

            barrelRotation.x = angleX;
            mainRotation.y = angleY;

            barrel.localEulerAngles = barrelRotation;
            frame.localEulerAngles = mainRotation;
        }

        [Serializable]
        public struct Limit
        {
            public float Max;
            public float Min;
        }
    }
}