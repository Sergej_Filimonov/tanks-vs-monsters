﻿using UnityEngine;

namespace GameSpace.Weapons
{
    public class MultiRocketLauncherView : WeaponView
    {
        [SerializeField] private Transform[] projectilePositions = null;

        private int indexPosition = 0;

        private void Awake()
        {
            ProjectileStartPosition = projectilePositions[indexPosition];
        }

        public override void Shot()
        {
            indexPosition++;
            if (indexPosition >= projectilePositions.Length)
            {
                indexPosition = 0;
            }
            ProjectileStartPosition = projectilePositions[indexPosition];
        }
    }
}