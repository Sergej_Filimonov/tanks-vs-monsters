﻿using UnityEngine;

namespace GameSpace.Weapons
{
    public class DoubleCannonView : WeaponView
    {
        [SerializeField] private Transform projectileStartPosition1 = null;
        [SerializeField] private Transform projectileStartPosition2 = null;

        private bool isSecondShot = false;

        private void Awake()
        {
            ProjectileStartPosition = projectileStartPosition1;
        }

        public override void Shot()
        {
            isSecondShot = !isSecondShot;
            ProjectileStartPosition = isSecondShot ? projectileStartPosition2 : projectileStartPosition1;
        }
    }
}