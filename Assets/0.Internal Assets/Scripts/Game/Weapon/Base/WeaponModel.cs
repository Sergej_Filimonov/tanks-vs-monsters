﻿using System;

namespace GameSpace.Weapons
{
    [Serializable]
    public class WeaponModel
    {
        public string WeaponName = "";
        public WeaponPlacementType PlacementType = WeaponPlacementType.Main;
        public float Damage = 100;
        public float RateOfFire = 60;
        public int ClipSize = 10;
        public float ReloadTime = 2;
    }
}