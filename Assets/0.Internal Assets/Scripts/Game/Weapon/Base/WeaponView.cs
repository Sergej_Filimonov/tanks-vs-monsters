﻿using UnityEngine;

namespace GameSpace.Weapons
{
    public abstract class WeaponView : MonoBehaviour
    {
        public WeaponRotor WeaponRotor { get; protected set; }
        public Transform ProjectileStartPosition { get; protected set; }

        public virtual void ApplyRotation(Transform source)
        {
            WeaponRotor.ApplyRotation(source);
        }

        public virtual void Shot()
        {

        }

        public virtual void Reload()
        {

        }
    }
}