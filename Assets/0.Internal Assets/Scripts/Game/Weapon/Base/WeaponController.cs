﻿using GameSpace.Projectiles;
using System;
using UniRx;
using UnityEngine;

namespace GameSpace.Weapons
{
    public abstract class WeaponController : IDisposable
    {
        public WeaponView View => view;
        public WeaponModel Model => model;

        protected WeaponView view;
        protected WeaponModel model;

        protected Projectile currentProjectile;
        protected int currentProjectileCount = 0;
        protected IDisposable rateOfFireCounter;
        protected IDisposable reloader;
        protected float rateOfFire = 0; //delay between shots
        protected float tempShotTime = 0;
        protected float tempReloadTime = 0;

        public WeaponController(WeaponView view, Projectile projectile, WeaponModel model)
        {
            this.view = view;
            this.model = model;

            ProjectilePools.Instance.CreateProjectilesPool(this, projectile, Mathf.FloorToInt(model.RateOfFire / 60.0f * 10));

            rateOfFire = 60.0f / model.RateOfFire;
            currentProjectileCount = model.ClipSize;
            rateOfFireCounter = Observable.EveryUpdate().Subscribe(_ => ShotsDelay());
        }

        public virtual void ApplyRotation(Transform source)
        {
            if (view.WeaponRotor != null) view.WeaponRotor.ApplyRotation(source);
        }

        public virtual void Shot()
        {
            if (tempShotTime <= 0 && currentProjectileCount > 0)
            {
                view.Shot();
                currentProjectile = ProjectilePools.Instance.GetProjectile(this);
                currentProjectile.transform.position = view.ProjectileStartPosition.position;
                currentProjectile.transform.rotation = view.ProjectileStartPosition.rotation;
                currentProjectile.Launch(this);
                currentProjectile.WorkEnded += OnProjectileWorkEnd;

                currentProjectileCount--;
                if (currentProjectileCount <= 0)
                {
                    Reload();
                }
                else
                {
                    tempShotTime = rateOfFire;
                }
            }
        }

        public virtual void Reload()
        {
            view.Reload();
            reloader = Observable.EveryUpdate().Subscribe(_ => ReloadProcess());
            tempReloadTime = model.ReloadTime;
        }

        private void OnProjectileWorkEnd(Projectile projectile)
        {
            projectile.WorkEnded -= OnProjectileWorkEnd;
            projectile.transform.localPosition = Vector3.zero;
            projectile.transform.rotation = Quaternion.identity;
            ProjectilePools.Instance.PutBackProjectile(this, projectile);
        }

        private void ShotsDelay()
        {
            if (tempShotTime > 0)
            {
                tempShotTime -= Time.deltaTime;
            }
        }

        private void ReloadProcess()
        {
            if (tempReloadTime > 0)
            {
                tempReloadTime -= Time.deltaTime;
            }
            else
            {
                OnReloadEnded();
            }
        }

        private void OnReloadEnded()
        {
            reloader?.Dispose();
            currentProjectileCount = model.ClipSize;
        }

        public virtual void Dispose()
        {
            rateOfFireCounter?.Dispose();
            reloader?.Dispose();
        }
    }
}