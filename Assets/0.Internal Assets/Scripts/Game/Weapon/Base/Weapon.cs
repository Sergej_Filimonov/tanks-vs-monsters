﻿using GameSpace.Projectiles;
using UnityEngine;

namespace GameSpace.Weapons
{
    [CreateAssetMenu(fileName = "Weapon", menuName = "Game Elements/Weapons/Weapon")]
    public class Weapon : ScriptableObject
    {
        [SerializeField] protected WeaponView view = null;
        [SerializeField] private WeaponModel model = new WeaponModel();
        [SerializeField] protected Projectile projectile = null;
        [SerializeField] protected WeaponControllerCreator creator = null;

        public WeaponModel Model { get => model; }

        public WeaponController CreateController(Transform weaponPosition)
        {
            return creator.CreateController(view, model, projectile, weaponPosition);
        }
    }
}