﻿using GameSpace.Projectiles;
using UnityEngine;

namespace GameSpace.Weapons
{
    public class WeaponControllerCreator : ScriptableObject
    {
        public virtual WeaponController CreateController(WeaponView view, WeaponModel data, Projectile projectile, Transform weaponPosition)
        {
            return null;
        }
    }
}