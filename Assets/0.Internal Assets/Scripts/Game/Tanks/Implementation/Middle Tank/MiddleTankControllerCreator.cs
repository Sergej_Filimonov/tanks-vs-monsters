﻿using UnityEngine;

namespace GameSpace.Tanks
{
    [CreateAssetMenu(fileName = "Middle Tank Controller Creator", menuName = "Game Elements/Tanks/Middle Tank Controller Creator")]
    public class MiddleTankControllerCreator : TankControllerCreator
    {
        public override TankController CreateController(TankView view, TankModel model)
        {
            return new MiddleTankController(view, model);
        }
    }
}