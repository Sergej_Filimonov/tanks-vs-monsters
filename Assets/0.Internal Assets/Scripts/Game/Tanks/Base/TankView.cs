﻿using UnityEngine;
using System;
using System.Collections.Generic;
using GameSpace.Weapons;
using Common.CachedCollidersSystem;
using GameSpace.Damageable;

namespace GameSpace.Tanks
{
    public abstract class TankView : MonoBehaviour, IDisposable, IDamageable, IColliderHolder<IDamageable>
    {
        [SerializeField] private new Collider collider = null;
        [SerializeField] private Transform frame = null;
        [SerializeField] private Transform tower = null;
        [SerializeField] private Transform pointer = null;
        [Space]
        [SerializeField] private LayerMask moveLayerMask = default;
        [SerializeField] private Transform frontPoint = null;
        [SerializeField] private Transform backPoint = null;
        [SerializeField] private WeaponPlace[] weaponPlaces = null;

        public event Action<DamageParameters> Damaged;

        public IReadOnlyDictionary<WeaponPlacementType, Transform> WeaponsPlaces => places;
        public Transform Frame { get; protected set; }
        public Transform Tower { get; protected set; }
        public Transform Pointer { get; protected set; }
        public Collider Collider { get; private set; }
        public IDamageable Component { get; private set; }

        protected Dictionary<WeaponPlacementType, Transform> places;

        private void Awake()
        {
            Frame = frame;
            Tower = tower;
            Pointer = pointer;

            places = new Dictionary<WeaponPlacementType, Transform>(weaponPlaces.Length);

            foreach (var place in weaponPlaces)
            {
                places.Add(place.PlacementType, place.Place);
            }

            Collider = collider;
            Component = this;
            CachedCollideWithComponent(this);
        }

        public void ApplayDamage(DamageParameters parameters)
        {
            Damaged?.Invoke(parameters);
        }

        public virtual void VisualiseDamage(DamageParameters parameters)
        {

        }

        public virtual void Death()
        {

        }

        public virtual bool CanMoveForward()
        {
            if (Physics.Raycast(frontPoint.position, frontPoint.forward, 0.2f, moveLayerMask))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public virtual bool CanMoveBack()
        {
            if (Physics.Raycast(backPoint.position, backPoint.forward, 0.2f, moveLayerMask))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public virtual void Dispose()
        {
            CachedColliders.Remove<IDamageable>(this);
        }

        public void CachedCollideWithComponent(IColliderHolder<IDamageable> holder)
        {
            CachedColliders.Add<IDamageable>(holder);
        }

        [Serializable]
        protected class WeaponPlace
        {
            [SerializeField] private WeaponPlacementType placementType = WeaponPlacementType.Main;
            [SerializeField] private Transform place = null;

            public WeaponPlacementType PlacementType { get => placementType; }
            public Transform Place { get => place; }
        }
    }
}