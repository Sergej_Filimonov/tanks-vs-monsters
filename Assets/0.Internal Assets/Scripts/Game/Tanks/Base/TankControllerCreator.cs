﻿using UnityEngine;

namespace GameSpace.Tanks
{
    public class TankControllerCreator : ScriptableObject
    {
        public virtual TankController CreateController(TankView view, TankModel model)
        {
            return null;
        }
    }
}