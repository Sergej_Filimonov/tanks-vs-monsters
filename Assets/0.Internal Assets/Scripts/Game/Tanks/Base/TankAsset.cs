﻿using UnityEngine;

namespace GameSpace.Tanks
{
    [CreateAssetMenu(fileName = "Tank", menuName = "Game Elements/Tanks/Tank")]
    public class TankAsset : ScriptableObject
    {
        [SerializeField] private TankView view = null;
        [SerializeField] private TankModel model = null;
        [SerializeField] private TankControllerCreator creator = null;

        public TankController CreateController(Transform tankPosition)
        {
            var viewInstance = GameObject.Instantiate(view, tankPosition.position, tankPosition.rotation, tankPosition);
            return creator.CreateController(viewInstance, model);
        }
    }
}