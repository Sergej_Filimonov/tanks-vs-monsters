﻿using System;
using GameSpace.Weapons;
using UnityEngine;

namespace GameSpace.Tanks
{
    [Serializable]
    public class TankModel
    {
        public int HP = 100;
        [Range(0.0f, 1.0f)]
        public float Armor = 0.5f;
        public float Speed = 60;
        public float RotationSpeed = 60;
        public Weapon[] Weapons = null;
    }
}