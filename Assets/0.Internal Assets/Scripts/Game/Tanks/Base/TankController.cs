﻿using GameSpace.Weapons;
using UnityEngine;
using System;
using GameSpace.Damageable;

namespace GameSpace.Tanks
{
    public abstract class TankController : IDisposable, IDamageable
    {
        public event Action<int> Damaged;
        public event Action<TankController> Dead;

        public TankView View => view;
        public WeaponController[] Weapons => weapons;
        public WeaponController CurrentWeapon => currentWeapon;
        public bool IsDead { get; protected set; }

        protected WeaponController[] weapons;
        protected WeaponController currentWeapon;
        protected int weaponIndex = 0;

        protected TankView view;
        protected TankModel model;
        protected int currentHP;

        protected Vector3 frameRotation;
        protected Vector3 towerRotation;

        public TankController(TankView view, TankModel model)
        {
            this.view = view;
            this.model = model;

            currentHP = model.HP;
            view.Damaged += ApplayDamage;

            SetupWeapons(model.Weapons);
        }

        private void SetupWeapons(Weapon[] weapons)
        {
            this.weapons = new WeaponController[weapons.Length];

            for (int i = 0; i < weapons.Length; i++)
            {
                if (view.WeaponsPlaces.ContainsKey(weapons[i].Model.PlacementType))
                {
                    this.weapons[i] = weapons[i].CreateController(view.WeaponsPlaces[weapons[i].Model.PlacementType]);
                }
            }

            currentWeapon = this.weapons[weaponIndex];
        }

        public virtual void Move(Vector2 moveVector)
        {
            if (moveVector.y != 0)
            {
                if (moveVector.y > 0)
                {
                    if (!view.CanMoveForward())
                    {
                        return;
                    }
                }
                else
                {
                    if (!view.CanMoveBack())
                    {
                        return;
                    }
                }
                view.Frame.position += view.Frame.forward * moveVector.y * model.Speed * Time.deltaTime;
                frameRotation = view.Frame.localEulerAngles;
                frameRotation.y += model.RotationSpeed * moveVector.x * Time.deltaTime * 0.7f;
                view.Frame.localEulerAngles = frameRotation;
            }
            else
            {
                frameRotation = view.Frame.localEulerAngles;
                frameRotation.y += model.RotationSpeed * moveVector.x * Time.deltaTime;
                view.Frame.localEulerAngles = frameRotation;
            }
        }

        public virtual void LookAtTarget(Vector3 targetPosition)
        {
            view.Pointer.LookAt(targetPosition);
            towerRotation = view.Pointer.localEulerAngles;
            towerRotation.x = 0;
            towerRotation.z = 0;
            view.Tower.localEulerAngles = towerRotation;
            currentWeapon.ApplyRotation(view.Pointer);
        }

        public virtual void Shot()
        {
            currentWeapon.Shot();
        }

        public virtual void SwitchWeapon(bool direction = true)
        {
            weaponIndex = direction ? weaponIndex + 1 : weaponIndex - 1;

            if (weaponIndex >= weapons.Length)
            {
                weaponIndex = 0;
            }
            else if (weaponIndex < 0)
            {
                weaponIndex = weapons.Length - 1;
            }

            currentWeapon = weapons[weaponIndex];
            Debug.Log("Weapon: " + CurrentWeapon.Model.WeaponName);
        }

        public void ApplayDamage(DamageParameters parameters)
        {
            if (currentHP <= 0) return;

            int damage = Mathf.RoundToInt(parameters.Damage * (1.0f - model.Armor));
            currentHP -= damage;
            Damaged?.Invoke(damage);

            if (currentHP <= 0)
            {
                Death();
            }
            else
            {
                view.VisualiseDamage(parameters);
            }
            Debug.Log("Tank apply damage. HP = " + currentHP);
        }

        protected virtual void Death()
        {
            IsDead = true;
            view.Death();
            Dead?.Invoke(this);
            Dead = null;
            Debug.Log("Tank is dead!");
        }

        public virtual void Dispose()
        {
            view.Dispose();
            foreach (var weapon in weapons)
            {
                weapon.Dispose();
            }
            Damaged = null;
            Dead = null;
        }
    }
}