﻿using UnityEngine;
using UniRx;
using System;

namespace GameSpace
{
    public class CameraMover : MonoBehaviour, IDisposable
    {
        [SerializeField] private Transform aim = null;
        [SerializeField] private float maxHeight = 20;
        [SerializeField] private float minHeight = 5;
        [SerializeField] private float resetDuration = 20f;

        public Transform Target { get; private set; }
        public Transform Target2 { get; private set; }

        private Vector3 newPosition;
        private Vector3 currentVelocity = Vector3.one * 10;
        private IDisposable update;

        public void SetTargets(Transform target1, Transform target2)
        {
            if (target1 != null && target2 != null)
            {
                Target = target1;
                Target2 = target2;
                StartWork();
            }
            else
            {
                update?.Dispose();
            }
        }

        private void StartWork()
        {
            update = Observable.EveryLateUpdate().Subscribe(_ => OnUpdate());
        }

        private void OnUpdate()
        {
            aim.position = Target.position + ((Target2.position - Target.position) / 2);
            newPosition = aim.position;
            newPosition.y = Mathf.Lerp(minHeight, maxHeight, Vector3.Distance(Target.position, Target2.position) / maxHeight);
            transform.position = Vector3.SmoothDamp(transform.position, newPosition, ref currentVelocity, resetDuration);
        }

        public void Dispose()
        {
            update?.Dispose();
        }
    }
}