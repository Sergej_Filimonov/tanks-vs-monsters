﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UniRx;

namespace GameSpace.InputSystem
{
    public class PlayerInput : MonoBehaviour, IDisposable
    {
        [SerializeField] private Transform aim = null;
        [SerializeField] private LayerMask layerMask = default;

        public event Action<Vector3> PointerInput;
        public event Action<Vector2> MoveInput;
        public event Action PointerDown;
        public event Action<bool> SwitchWeapon;

        public Transform Pointer => aim;

        private Camera gameCamera;
        private Vector3 pointerPosition;
        private Ray ray;
        private Vector2 moveInput = new Vector2();

        public void Init()
        {
            gameCamera = Camera.main;
            Observable.EveryUpdate().Subscribe(_ => MyUpdate()).AddTo(this);
        }

        private void MyUpdate()
        {
            pointerPosition = Input.mousePosition;
            ray = gameCamera.ScreenPointToRay(pointerPosition);
            if (Physics.Raycast(gameCamera.transform.position, 
                ray.origin - gameCamera.transform.position, 
                out RaycastHit raycastHit, 1000, layerMask))
            {
                aim.position = raycastHit.point;
            }
            PointerInput?.Invoke(aim.position);

            moveInput.x = Input.GetAxis("Horizontal");
            moveInput.y = Input.GetAxis("Vertical");
            MoveInput?.Invoke(moveInput);

            if (Input.GetKeyDown(KeyCode.E))
            {
                SwitchWeapon?.Invoke(true);
            }

            if (Input.GetKeyDown(KeyCode.Q))
            {
                SwitchWeapon?.Invoke(false);
            }

            if (Input.GetMouseButton(0))
            {
                PointerDown?.Invoke();
            }
        }

        public void Dispose()
        {
            PointerInput = null;
            MoveInput = null;
            PointerDown = null;
        }

        private void OnDestroy()
        {
            Dispose();
        }
    }
}