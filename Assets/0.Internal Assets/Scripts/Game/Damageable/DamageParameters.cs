﻿namespace GameSpace.Damageable
{
    public class DamageParameters
    {
        public float Damage { get; private set; }

        public DamageParameters(float damage)
        {
            Damage = damage;
        }
    }
}