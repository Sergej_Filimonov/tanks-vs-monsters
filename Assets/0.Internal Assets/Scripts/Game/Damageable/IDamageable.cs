﻿namespace GameSpace.Damageable
{
    public interface IDamageable
    {
        void ApplayDamage(DamageParameters parameters);
    }
}