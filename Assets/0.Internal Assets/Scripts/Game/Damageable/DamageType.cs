﻿using System;

namespace GameSpace.Damageable
{
    [Serializable]
    public enum DamageType
    {
        Piercing,
        Impact,
        Fire
    }
}