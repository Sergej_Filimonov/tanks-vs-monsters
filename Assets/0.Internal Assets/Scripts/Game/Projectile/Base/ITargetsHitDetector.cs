﻿using GameSpace.Damageable;
using System.Collections.Generic;

namespace GameSpace.Projectiles
{
    public interface ITargetsHitDetector
    {
        List<IDamageable> DetectTargets();
    }
}