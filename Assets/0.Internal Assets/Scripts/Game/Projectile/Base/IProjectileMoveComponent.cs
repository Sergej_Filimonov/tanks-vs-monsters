﻿using System;

namespace GameSpace.Projectiles
{
    public interface IProjectileMoveComponent : IDisposable
    {
        void Init(Projectile owner);
        void StarMove();
        void OnHit();
        void Stop();
    }
}