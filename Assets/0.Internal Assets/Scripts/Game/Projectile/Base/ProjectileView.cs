﻿using UnityEngine;
using System;

namespace GameSpace.Projectiles
{
    public abstract class ProjectileView : MonoBehaviour, IDisposable
    {
        public abstract void Init();
        public abstract void OnLaunch();
        public abstract void OnHit(HitResult result);

        public virtual void Dispose()
        {
        }
    }
}