﻿using System;
using UnityEngine;

namespace GameSpace.Projectiles
{
    public interface IProjectileHitComponent : IDisposable
    {
        event Action<HitResult> Hit;
        event Action WorkEnded;

        void Init(Projectile owner);
        void StartWork();
        void BlowUpNow(Vector3 position);
    }
}