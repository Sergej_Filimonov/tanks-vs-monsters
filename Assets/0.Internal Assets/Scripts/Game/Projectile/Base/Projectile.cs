﻿using Common.Pool;
using GameSpace.Damageable;
using GameSpace.Weapons;
using System;
using UniRx;
using UnityEngine;

namespace GameSpace.Projectiles
{
    public abstract class Projectile : MonoBehaviour, IDisposable, IPoolItem
    {
        [SerializeField] protected ProjectileView view = null;

        public event Action<HitResult> Hit;
        public event Action<Projectile> WorkEnded;

        protected IProjectileMoveComponent moveComponent;
        protected IProjectileHitComponent hitComponent;

        protected WeaponController weapon;
        protected IDisposable destroyTimer;

        public virtual void Init()
        {
            moveComponent = GetComponent<IProjectileMoveComponent>();
            hitComponent = GetComponent<IProjectileHitComponent>();

            view.Init();
            moveComponent.Init(this);
            hitComponent.Init(this);

            hitComponent.Hit += OnHit;
            hitComponent.WorkEnded += OnHitComponentWorkEnded;
        }

        public virtual void Launch(WeaponController weapon)
        {
            this.weapon = weapon;

            view.OnLaunch();
            moveComponent.StarMove();
            hitComponent.StartWork();

            destroyTimer = Observable.Timer(TimeSpan.FromSeconds(7.0f)).Subscribe(l => BlowUpNow()).AddTo(this);
        }

        public virtual void BlowUpNow()
        {
            hitComponent.BlowUpNow(transform.position);
        }

        public virtual void Stop()
        {
            moveComponent.Stop();
        }

        protected virtual void OnHit(HitResult result)
        {
            moveComponent.OnHit();
            view.OnHit(result);

            foreach (var damageble in result.Targets)
            {
                damageble.ApplayDamage(new DamageParameters(weapon.Model.Damage));
            }

            Hit?.Invoke(result);
        }

        public virtual void OnHitComponentWorkEnded()
        {
            destroyTimer.Dispose();
            moveComponent.Stop();
            WorkEnded?.Invoke(this);
        }

        public virtual void Dispose()
        {
            view.Dispose();
            moveComponent.Dispose();
            hitComponent.Dispose();
            Hit = null;
            WorkEnded = null;
            destroyTimer?.Dispose();
        }

        protected virtual void OnDestroy()
        {
            Dispose();
        }
    }
}