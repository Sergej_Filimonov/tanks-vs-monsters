﻿using Common.Effects;
using Common.Pool;
using System;
using UniRx;
using UnityEngine;

namespace GameSpace.Projectiles
{
    public class SimpleProjectileView : ProjectileView
    {
        [SerializeField] private Effect hitEffect = null;

        public override void Init()
        {
            if (!ObjectsPools.Instance.HasPool(PoolType.ProjectileHitEffects))
            {
                ObjectsPools.Instance.AddPool(PoolType.ProjectileHitEffects, ObjectsPools.Instance.transform);
            }
        }

        public override void OnHit(HitResult result)
        {
            var hitEffectInstance = ObjectsPools.Instance.Spawn(PoolType.ProjectileHitEffects, hitEffect);
            hitEffectInstance.transform.position = transform.position;
            hitEffectInstance.Play();
            Observable.Timer(TimeSpan.FromSeconds(hitEffect.Duration)).Subscribe(_ => ObjectsPools.Instance.Despawn(PoolType.ProjectileHitEffects, hitEffectInstance)).AddTo(this);
        }

        public override void OnLaunch()
        {
        }
    }
}