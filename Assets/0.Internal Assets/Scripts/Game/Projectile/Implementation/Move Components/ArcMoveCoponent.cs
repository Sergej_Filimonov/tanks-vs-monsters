﻿using Common.Architecture.Templates;
using System;
using UniRx;
using UnityEngine;

namespace GameSpace.Projectiles
{
    public class ArcMoveCoponent : MonoBehaviour, IProjectileMoveComponent
    {
        [SerializeField] private float maxHeight = 10;
        [SerializeField] private float speed = 1;

        private IDisposable update;
        private Vector3 startPosition;
        private Vector3 targetPosition;
        private Vector3 currentPosition;
        private float timeToMove;
        private float tempTime;
        private float progress;

        public void Init(Projectile owner)
        {
        }

        public void StarMove()
        {
            update = Observable.EveryUpdate().Subscribe(_ => MyUpdate());
            startPosition = transform.position;
            targetPosition = ApplicationManager.Instance.Game.Controller.SceneData.Input.Pointer.position;
            timeToMove = Vector3.Distance(startPosition, targetPosition) / speed;
            tempTime = 0;
        }

        private void MyUpdate()
        {
            progress = tempTime / timeToMove;
            transform.forward = Vector3.Lerp(Vector3.up, Vector3.down, progress);
            currentPosition = Vector3.Lerp(startPosition, targetPosition, progress);
            currentPosition.y = Mathf.Lerp(startPosition.y, targetPosition.y, progress) + (maxHeight * Mathf.PingPong(progress, 0.5f));
            transform.position = currentPosition;
            tempTime += Time.deltaTime;
        }

        public void OnHit()
        {
            Stop();
        }

        public void Stop()
        {
            update?.Dispose();
        }

        public void Dispose()
        {
            Stop();
        }
    }
}