﻿using System;
using UniRx;
using UnityEngine;

namespace GameSpace.Projectiles
{
    public class LineMoveComponent : MonoBehaviour, IProjectileMoveComponent
    {
        [SerializeField] private float speed = 10.0f;
        [SerializeField] private bool useRange = false;
        [SerializeField] private float range = 50.0f;

        private IDisposable update;
        private float tempRange = 0;
        
        public void Init(Projectile owner)
        {
            
        }

        public void StarMove()
        {
            tempRange = 0;
            update = Observable.EveryUpdate().Subscribe(_ => MyUpdate());
        }

        private void MyUpdate()
        {
            tempRange += speed * Time.deltaTime;
            transform.position += transform.forward * tempRange;
            if (tempRange >= range)
            {
                if (useRange)
                    Stop();
            }
        }

        public void OnHit()
        {
            if (!useRange) 
                Stop();
        }

        public void Stop()
        {
            update?.Dispose();
            tempRange = 0;
        }

        public void Dispose()
        {
            update?.Dispose();
        }
    }
}