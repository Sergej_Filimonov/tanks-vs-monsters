﻿using UnityEngine;

namespace GameSpace.Projectiles
{
    [RequireComponent(typeof(Rigidbody))]
    public class PhysicsProjectileMoveCoponent : MonoBehaviour, IProjectileMoveComponent
    {
        [SerializeField] private float force = 100;

        private Projectile owner;
        private Rigidbody rgbody;

        public void Init(Projectile owner)
        {
            this.owner = owner;
            rgbody = GetComponent<Rigidbody>();
        }

        public void StarMove()
        {
            rgbody.velocity = Vector3.zero;
            rgbody.AddRelativeForce(transform.forward * force, ForceMode.Impulse);
        }

        public void OnHit()
        {
            Stop();
        }

        public void Stop()
        {
            rgbody.velocity = Vector3.zero;
        }

        public void Dispose()
        {

        }
    }
}