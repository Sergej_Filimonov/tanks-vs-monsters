﻿using Common.CachedCollidersSystem;
using GameSpace.Damageable;
using System;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace GameSpace.Projectiles
{
    [RequireComponent(typeof(Collider), typeof(ObservableTriggerTrigger))]
    public class TimedTriggerHitComponent : MonoBehaviour, IProjectileHitComponent
    {
        [SerializeField] private float workTime = 3.0f;
        [SerializeField] private float workInterval = 0.2f;

        public event Action<HitResult> Hit;
        public event Action WorkEnded;

        private ObservableTriggerTrigger triggerTrigger;
        private HashSet<Collider> passedTargets = new HashSet<Collider>();
        private IDisposable workTimer;
        private IDisposable workTrigger;
        private HitResult hitResult;
        private List<IDamageable> targets;

        public void Init(Projectile owner)
        {
            triggerTrigger = GetComponent<ObservableTriggerTrigger>();
            hitResult = new HitResult(owner);
            targets = new List<IDamageable>();
        }

        public void StartWork()
        {
            workTimer = Observable.Timer(TimeSpan.FromSeconds(workTime)).Subscribe(_ => StopWork());
            workTrigger = triggerTrigger.OnTriggerEnterAsObservable().Subscribe(OnObservableTriggerEnter);
        }

        private void OnObservableTriggerEnter(Collider collider)
        {
            if (CachedColliders.Get<IDamageable>(collider, out IDamageable damageable))
            {
                hitResult.Point = collider.bounds.center;
                targets.Clear();
                targets.Add(damageable);
                hitResult.Targets = targets;
                Hit?.Invoke(hitResult);
            }
        }

        private void StopWork()
        {
            workTimer.Dispose();
            workTrigger.Dispose();
            WorkEnded?.Invoke();
        }

        public void BlowUpNow(Vector3 position)
        {
            StopWork();
        }

        public void Dispose()
        {
            workTimer?.Dispose();
            workTrigger?.Dispose();
            Hit = null;
            WorkEnded = null;
        }
    }
}