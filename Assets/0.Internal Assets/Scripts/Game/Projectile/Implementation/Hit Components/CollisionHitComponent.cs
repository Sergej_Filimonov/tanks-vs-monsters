﻿using System;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace GameSpace.Projectiles
{
    [RequireComponent(typeof(Collider), typeof(ObservableCollisionTrigger))]
    public class CollisionHitComponent : MonoBehaviour, IProjectileHitComponent
    {
        public event Action<HitResult> Hit;
        public event Action WorkEnded;

        private ITargetsHitDetector targetsDetector;
        private HitResult hitResult;

        public void Init(Projectile owner)
        {
            targetsDetector = GetComponent<ITargetsHitDetector>();
            GetComponent<ObservableCollisionTrigger>().OnCollisionEnterAsObservable().Subscribe(OnCollision);
            hitResult = new HitResult(owner);
        }

        public void StartWork()
        {

        }

        private void OnCollision(Collision collision)
        {
            hitResult.Normal = collision.contacts[0].normal;
            BlowUpNow(collision.contacts[0].point);
        }

        public void BlowUpNow(Vector3 position)
        {
            hitResult.Point = position;
            hitResult.Targets = targetsDetector.DetectTargets();
            Hit?.Invoke(hitResult);
            WorkEnded?.Invoke();
        }

        public void Dispose()
        {
            Hit = null;
            WorkEnded = null;
        }
    }
}