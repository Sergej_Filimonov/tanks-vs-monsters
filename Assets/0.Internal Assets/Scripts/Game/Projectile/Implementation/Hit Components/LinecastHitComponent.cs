﻿using System;
using UniRx;
using UnityEngine;

namespace GameSpace.Projectiles
{
    public class LinecastHitComponent : MonoBehaviour, IProjectileHitComponent
    {
        [SerializeField] private LayerMask layerMask = default;

        public event Action<HitResult> Hit;
        public event Action WorkEnded;

        private ITargetsHitDetector targetsDetector;
        private HitResult hitResult;
        private IDisposable update;
        private IDisposable lateUpdate;
        private Vector3 lastPosition;

        public void Init(Projectile owner)
        {
            targetsDetector = GetComponent<ITargetsHitDetector>();
            hitResult = new HitResult(owner);
        }

        public void StartWork()
        {
            update = Observable.EveryUpdate().Subscribe(_ => MyUpdate());
            lateUpdate = Observable.EveryUpdate().Subscribe(_ => MyLateUpdate());
            lastPosition = transform.position;
        }

        private void MyUpdate()
        {
            if (Physics.Linecast(lastPosition, transform.position, out RaycastHit raycastHit, layerMask))
            {
                OnCollision(raycastHit);
            }
        }

        private void MyLateUpdate()
        {
            lastPosition = transform.position;
        }

        private void OnCollision(RaycastHit raycastHit)
        {
            update.Dispose();
            lateUpdate.Dispose();
            hitResult.Normal = raycastHit.normal;
            BlowUpNow(raycastHit.point);
        }

        public void BlowUpNow(Vector3 position)
        {
            hitResult.Point = position;
            hitResult.Targets = targetsDetector.DetectTargets();
            Hit?.Invoke(hitResult);
            WorkEnded?.Invoke();
        }

        public void Dispose()
        {
            Hit = null;
            WorkEnded = null;
            update?.Dispose();
            lateUpdate?.Dispose();
        }
    }
}