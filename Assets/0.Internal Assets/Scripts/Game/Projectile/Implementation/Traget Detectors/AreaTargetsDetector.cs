﻿using GameSpace.Damageable;
using System.Collections.Generic;
using UnityEngine;
using Common.CachedCollidersSystem;

namespace GameSpace.Projectiles
{
    public class AreaTargetsDetector : MonoBehaviour, ITargetsHitDetector
    {
        [SerializeField] private float radius = 3.0f;
        [SerializeField] private LayerMask layerMask = default;

        private Collider[] colliders;
        private List<IDamageable> targets = new List<IDamageable>();

        public List<IDamageable> DetectTargets()
        {
            colliders = Physics.OverlapSphere(transform.position, radius, layerMask);

            if (colliders.Length > 0)
            {
                for (int i = 0; i < colliders.Length; i++)
                {
                    if (CachedColliders.Get<IDamageable>(colliders[i], out IDamageable damageable))
                    {
                        if (ColliderIsReachable(colliders[i]))
                        {
                            targets.Add(damageable);
                        }
                    }
                }
            }

            return targets;
        }

        private bool ColliderIsReachable(Collider collider)
        {
            bool result = false;

            if (Vector3.Distance(transform.position, collider.bounds.ClosestPoint(transform.position)) > 0.1f)
            {
                if (Physics.Linecast(transform.position, collider.bounds.center, out RaycastHit hitResult, layerMask))
                {
                    if (hitResult.collider == collider)
                    {
                        result = true;
                    }
                }
            }
            else
            {
                result = true;
            }

            return result;
        }
    }
}