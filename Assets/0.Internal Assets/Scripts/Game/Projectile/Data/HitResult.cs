﻿using GameSpace.Damageable;
using System.Collections.Generic;
using UnityEngine;

namespace GameSpace.Projectiles
{
    public class HitResult
    {
        public Vector3 Point { get; set; }
        public Vector3 Normal { get; set; } = Vector3.up;
        public Projectile Projectile { get; set; }
        public List<IDamageable> Targets { get; set; }

        public HitResult(Projectile projectile)
        {
            Projectile = projectile;
        }
    }
}