﻿using Common.Pool;
using System;
using TMPro;
using UniRx;
using UnityEngine;

namespace GameSpace.Views
{
    public class DamageView : MonoBehaviour
    {
        [SerializeField] private TMP_Text text = null;
        [SerializeField] private AnimationClip animationClip = null;

        public void Show(int damage, Color color)
        {
            text.color = color;
            text.text = damage.ToString();
            Observable.Timer(TimeSpan.FromSeconds(animationClip.length)).
                Subscribe(_ => ObjectsPools.Instance.Despawn(PoolType.Views, this)).
                AddTo(this);
        }
    }
}