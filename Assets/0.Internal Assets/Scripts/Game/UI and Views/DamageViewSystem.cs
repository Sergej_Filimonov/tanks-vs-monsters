﻿using Common.Pool;
using UnityEngine;

namespace GameSpace.Views
{
    public class DamageViewSystem : MonoBehaviour
    {
        [SerializeField] private DamageView prefab = null;

        private void Awake()
        {
            ObjectsPools.Instance.AddPool(PoolType.Views, ObjectsPools.Instance.transform);
        }

        public void Show(Vector3 position, int damage, Color color)
        {
            var view = ObjectsPools.Instance.Spawn(PoolType.Views, prefab);
            view.transform.position = position;
            view.Show(damage, color);
        }
    }
}