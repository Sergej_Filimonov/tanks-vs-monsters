﻿using System;
using UniRx;
using UnityEngine;

namespace GameSpace.Monsters
{
    public class SimpleLineMoveComponent : MonoBehaviour, IMonsterMoveComponent
    {
        [SerializeField] private float speed = 5.0f;
        [SerializeField] private float forwardOffset = 1.0f;
        [SerializeField] private float targetOffset = 1.0f;
        [SerializeField] private Transform pivot = null;
        [SerializeField] private new Rigidbody rigidbody = null;

        public event Action StartMoving;
        public event Action StopMoving;
        public event Action TargetReached;

        public bool isMoving { get; private set; } = false;

        private Transform target;
        private IDisposable moving;
        private IDisposable tracking;

        public void StartMove(Transform target)
        {
            this.target = target;
            rigidbody.isKinematic = true;
            pivot.LookAt(target);
            isMoving = true;
            moving?.Dispose();
            tracking?.Dispose();
            moving = Observable.EveryUpdate().Subscribe(frams => Moving());
            StartMoving?.Invoke();
        }

        private void TargetTracker()
        {
            pivot.LookAt(target);
            if (Vector3.Distance(pivot.position, target.position) > forwardOffset + targetOffset)
            {
                isMoving = true;
                moving = Observable.EveryUpdate().Subscribe(frams => Moving());
                StartMoving?.Invoke();
                tracking.Dispose();
            }
        }

        private void Moving()
        {
            pivot.position = Vector3.MoveTowards(pivot.position, target.position, speed * Time.deltaTime);
            pivot.LookAt(target);
            if (Vector3.Distance(pivot.position, target.position) < forwardOffset)
            {
                Stop();
                tracking = Observable.EveryUpdate().Subscribe(frams => TargetTracker());
                TargetReached?.Invoke();
            }
        }

        public void Stop()
        {
            isMoving = false;
            rigidbody.isKinematic = false;
            moving?.Dispose();
            tracking?.Dispose();
            StopMoving?.Invoke();
        }

        public void Dispose()
        {
            moving?.Dispose();
            tracking?.Dispose();
            StartMoving = null;
            StopMoving = null;
            TargetReached = null;
        }
    }
}