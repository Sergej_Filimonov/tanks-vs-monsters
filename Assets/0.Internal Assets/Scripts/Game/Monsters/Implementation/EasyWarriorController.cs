﻿namespace GameSpace.Monsters
{
    public class EasyWarriorController : MonsterController 
    {
        public EasyWarriorController(MonsterModel model, MonsterView view) : base(model, view)
        {
            attackComponent = new NearLineAttackComponent();
            attackComponent.Init(this);
        }
    }
}