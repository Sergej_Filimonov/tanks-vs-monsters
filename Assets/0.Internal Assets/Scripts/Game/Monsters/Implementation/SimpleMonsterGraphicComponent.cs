﻿using GameSpace.Damageable;
using UnityEngine;

namespace GameSpace.Monsters
{
    public class SimpleMonsterGraphicComponent : MonoBehaviour, IMonsterGraphicComponent
    {
        [SerializeField] private Animator animator = null;

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Attack()
        {
            animator.SetTrigger("Attack");
        }

        public void Death()
        {
            animator.SetBool("isMoving", false);
            gameObject.SetActive(false);
        }

        public void Move()
        {
            animator.SetBool("isMoving", true);
        }

        public void StopMove()
        {
            animator.SetBool("isMoving", false);
        }

        public void VisualiseDamage(DamageParameters parameters)
        {
        }

        public void Dispose()
        {
        }
    }
}