﻿using Common.CachedCollidersSystem;
using GameSpace.Damageable;
using GameSpace.Monsters;
using UnityEngine;

public class NearLineAttackComponent : IMonsterAttackComponent
{
    private const float ATTACK_RANGE = 1.5f;

    private MonsterController controller;

    public void Init(MonsterController controller)
    {
        this.controller = controller;
    }

    public void Attack()
    {
        var hits = Physics.RaycastAll(controller.View.StartAttackPoint.position, 
            controller.View.StartAttackPoint.forward, 
            ATTACK_RANGE, 
            controller.View.AttackLayerMask);

        if (hits.Length > 0)
        {
            for (int i = 0; i < hits.Length; i++)
            {
                if (CachedColliders.Get<IDamageable>(hits[i].collider, out IDamageable damageable))
                {
                    damageable.ApplayDamage(new DamageParameters(controller.Model.Damage));
                }
            }
        }
    }
}
