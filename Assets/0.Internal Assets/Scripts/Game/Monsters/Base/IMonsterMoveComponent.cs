﻿using System;
using UnityEngine;

namespace GameSpace.Monsters
{
    public interface IMonsterMoveComponent: IDisposable
    {
        event Action StartMoving;
        event Action StopMoving;
        event Action TargetReached;
        bool isMoving { get; }
        void StartMove(Transform target);
        void Stop();
    }
}