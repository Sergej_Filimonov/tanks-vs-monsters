﻿namespace GameSpace.Monsters
{
    public enum MonsterType
    {
        EasyWarrior,
        MediumWarrior,
        HeavyWarrior
    }
}