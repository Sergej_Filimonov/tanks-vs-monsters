﻿using Common.CachedCollidersSystem;
using Common.Pool;
using GameSpace.Damageable;
using System;
using UnityEngine;

namespace GameSpace.Monsters
{
    [RequireComponent(typeof(Collider))]
    public class MonsterView : MonoBehaviour, IDamageable, IColliderHolder<IDamageable>, IDisposable, IPoolItem
    {
        [SerializeField] private Transform startAttackPoint = null;
        [SerializeField] private LayerMask attackLayerMask = default;

        public event Action TargetReached;
        public event Action<DamageParameters> Damaged;
        public event Action<MonsterView> Destroyed;

        public Collider Collider { get; private set; }
        public IDamageable Component { get; private set; }
        public MonsterModel Model => model;
        public Transform StartAttackPoint => startAttackPoint;
        public LayerMask AttackLayerMask => attackLayerMask;
        public bool IsMoving => moveComponent.isMoving;

        private MonsterModel model;
        private IMonsterMoveComponent moveComponent;
        private IMonsterGraphicComponent graphicComponent;

        public void Init()
        {
            moveComponent = GetComponent<IMonsterMoveComponent>();
            graphicComponent = GetComponent<IMonsterGraphicComponent>();

            InitTargetDetector();

            Collider = GetComponent<Collider>();
            Component = this;
            CachedCollideWithComponent(this);
        }

        public void Show(MonsterModel model)
        {
            this.model = model;
            graphicComponent.Show();
        }

        public void StarMove(Transform target)
        {
            moveComponent.StartMove(target);
            graphicComponent.Move();
        }

        public void StopMove()
        {
            graphicComponent.StopMove();
            moveComponent.Stop();
        }

        public void Attack()
        {
            graphicComponent.Attack();
        }

        public void Death()
        {
            StopMove();
            graphicComponent.Death();
            Destroyed?.Invoke(this);
        }

        public void CachedCollideWithComponent(IColliderHolder<IDamageable> holder)
        {
            CachedColliders.Add<IDamageable>(holder);
        }

        public void ApplayDamage(DamageParameters parameters)
        {
            Damaged?.Invoke(parameters);
        }

        public void VisualiseDamage(DamageParameters parameters)
        {
            graphicComponent.VisualiseDamage(parameters);
        }

        public void Dispose()
        {
            Damaged = null;
            Destroyed = null;
            moveComponent.Dispose();
            graphicComponent.Dispose();
        }

        protected virtual void InitTargetDetector()
        {
            moveComponent.TargetReached += () => TargetReached?.Invoke();
        }

        private void OnDestroy()
        {
            CachedColliders.Remove<IDamageable>(this);
        }
    }
}