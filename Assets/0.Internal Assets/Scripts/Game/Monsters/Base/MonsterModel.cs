﻿using System;

namespace GameSpace.Monsters
{
    [Serializable]
    public class MonsterModel
    {
        public MonsterType Type { get; set; }
        public int Hp { get; set; }
        public float Armor { get; set; }
        public float Damage { get; set; }
        public float AttackSpeed { get; set; }
    }
}