﻿using UnityEngine;

namespace GameSpace.Monsters
{
    public interface IMonsterAttackComponent
    {
        void Init(MonsterController controller);

        void Attack();
    }
}