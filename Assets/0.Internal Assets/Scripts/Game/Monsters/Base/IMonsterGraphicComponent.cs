﻿using GameSpace.Damageable;
using System;

namespace GameSpace.Monsters
{
    public interface IMonsterGraphicComponent : IDisposable
    {
        void Show();
        void Attack();
        void VisualiseDamage(DamageParameters parameters);
        void Move();
        void StopMove();
        void Death();
    }
}