﻿using Common.Architecture.Templates;
using GameSpace.Damageable;
using System;
using UnityEngine;
using UniRx;

namespace GameSpace.Monsters
{
    public abstract class MonsterController : IDisposable
    {
        public event Action<int> Damaged;
        public event Action<MonsterController> Dead;

        public MonsterModel Model => model;
        public MonsterView View => view;
        public bool IsDead = false;

        protected MonsterModel model;
        protected MonsterView view;
        protected IMonsterAttackComponent attackComponent;
        protected Transform attackTarget;
        private IDisposable attackTimer;

        public MonsterController (MonsterModel model, MonsterView view)
        {
            this.model = model;
            this.view = view;
            view.Damaged += ApplyDamage;
            view.TargetReached += OnTargetReached;
        }

        public void Start(Transform startPoint)
        {
            view.transform.position = startPoint.position;
            view.transform.rotation = startPoint.rotation;
            view.Show(model);

            FindAttackTarget();

            view.StarMove(attackTarget);
        }

        protected virtual void FindAttackTarget()
        {
            attackTarget = ApplicationManager.Instance.Game.Controller.TankController.View.Frame;
        }

        protected virtual void OnTargetReached()
        {
            Attack();
        }

        protected virtual void Attack()
        {
            if (IsDead) return;
            if (view.IsMoving) return;
            attackComponent.Attack();
            view.Attack();
            attackTimer = Observable.Timer(TimeSpan.FromSeconds(60.0f / model.AttackSpeed)).Subscribe(_ => Attack());
        }

        public virtual void ApplyDamage(DamageParameters parameters)
        {
            if (model.Hp <= 0) return;

            int damage = Mathf.RoundToInt(parameters.Damage * (1.0f - model.Armor));
            model.Hp -= damage;
            Damaged?.Invoke(damage);

            if (model.Hp <= 0)
            {
                Death();
            }
            else
            {
                view.VisualiseDamage(parameters);
            }
        }

        public virtual void Death()
        {
            IsDead = true;
            view.Death();
            view = null;
            Damaged = null;
            Dead?.Invoke(this);
            Dead = null;
        }

        public void Dispose()
        {
            if (view != null)
            {
                view.Damaged -= ApplyDamage;
                view.TargetReached -= OnTargetReached;
                view.Dispose();
            }
            attackTimer?.Dispose();
            Damaged = null;
            Dead = null;
        }
    }
}