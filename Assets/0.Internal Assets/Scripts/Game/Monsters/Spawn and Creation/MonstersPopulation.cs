﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace GameSpace.Monsters
{
    public class MonstersPopulation : IDisposable
    {
        public event Action<MonsterController, int> MonsterDamaged;
        public List<MonsterController> Controllers => controllers;

        private List<MonsterController> controllers = new List<MonsterController>();
        private MonstersSpawner spawner;
        private bool isWorcking = true;

        public MonstersPopulation(MonstersSpawner spawner)
        {
            this.spawner = spawner;
            this.spawner.MonsterSpawned += OnMonsterCreated;
        }

        public void SartWork()
        {
            isWorcking = true;
            var models = MonsterModelsGenerator.GetModelsList(10);
            spawner.StartSpawn(models);
        }

        public void StopWork()
        {
            isWorcking = false;
            this.spawner.MonsterSpawned -= OnMonsterCreated;
        }

        private void OnMonsterCreated(MonsterController monsterController)
        {
            controllers.Add(monsterController);
            monsterController.Damaged += d => MonsterDamaged?.Invoke(monsterController, d);
            monsterController.Dead += OnMonsterDead;
        }

        private void OnMonsterDead(MonsterController monsterController)
        {
            controllers.Remove(monsterController);
            monsterController.Dispose();
            if (isWorcking)
            {
                var models = MonsterModelsGenerator.GetModelsList(1);
                spawner.StartSpawn(models);
            }
        }

        public void Dispose()
        {
            foreach (var controller in controllers)
            {
                controller.Dispose();
            }
            controllers.Clear();
        }
    }
}