﻿using System.Collections.Generic;
using UnityEngine;

namespace GameSpace.Monsters
{
    public static class MonsterModelsGenerator
    {
        public static List<MonsterModel> GetModelsList(int count)
        {
            var output = new List<MonsterModel>(count);

            for (int i = 0; i < count; i++)
            {
                float spawnDelay = Random.Range(2.0f, 60.0f);
                int spawnPointIndex = Random.Range(0, 10);
                var monsterType = MonsterModelsFactory.GetRandomType();

                var model = MonsterModelsFactory.CreateModel(monsterType, spawnDelay, spawnPointIndex);
                output.Add(model);
            }

            return output;
        }
    }
}