﻿using System;
using UnityEngine;


namespace GameSpace.Monsters
{
    [Serializable]
    public class MonsterTypeViewPair
    {
        [SerializeField] private MonsterType type = MonsterType.EasyWarrior;
        [SerializeField] private MonsterView view = null;

        public MonsterType Type { get => type; }
        public MonsterView View { get => view; }
    }
}