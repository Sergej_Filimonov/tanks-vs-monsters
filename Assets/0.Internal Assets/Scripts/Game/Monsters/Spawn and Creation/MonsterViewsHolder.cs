﻿using Common.Pool;
using System.Collections.Generic;
using UnityEngine;

namespace GameSpace.Monsters
{
    public class MonsterViewsHolder
    {
        public GameObject ViewsPoolParent { get; set; }

        private Dictionary<MonsterType, IPool<MonsterView>> pools = new Dictionary<MonsterType, IPool<MonsterView>>();

        public MonsterViewsHolder(Dictionary<MonsterType, MonsterView> data)
        {
            ViewsPoolParent = new GameObject("[POOL] - Enemy views");
            foreach (var pair in data)
            {
                var pool = PoolHelper.CreatePool(pair.Value, p => { p.Init(); }, ViewsPoolParent.transform, 5);
                pools.Add(pair.Key, pool);
            }
        }

        public MonsterView GetMonsterView(MonsterModel model)
        {
            if (pools.ContainsKey(model.Type))
            {
                return pools[model.Type].GetItem();
            }
            else
            {
                throw new System.Exception("No view for given enemy type!");
            }
        }

        public void PutBackView(MonsterModel model, MonsterView view)
        {
            pools[model.Type].PutBack(view);
        }
    }
}