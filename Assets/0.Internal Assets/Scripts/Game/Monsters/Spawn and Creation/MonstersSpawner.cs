﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using Random = UnityEngine.Random;

namespace GameSpace.Monsters
{
    public class MonstersSpawner : IDisposable
    {
        public event Action LastMonsterSpawned;
        public event Action<MonsterController> MonsterSpawned;

        private List<Transform> points;
        private MonsterViewsHolder viewsHolder;
        private CompositeDisposable disposables;

        public MonstersSpawner(List<Transform> points, MonsterViewsHolder viewsHolder)
        {
            this.points = points;
            this.viewsHolder = viewsHolder;
            disposables = new CompositeDisposable();
        }

        public void StartSpawn(List<MonsterModel> models)
        {
            int currentModelIndex = 0;

            IDisposable workCycle = null;
            workCycle = Observable.Interval(TimeSpan.FromSeconds(1.0f)).
                Subscribe(frameIndex => 
                {
                    var model = models[currentModelIndex];
                    var startPoint = GetStartPoint(Random.Range(0, 100));
                    SpawnEnemy(model, startPoint);
                    ++currentModelIndex;

                    if (currentModelIndex >= models.Count)
                    {
                        disposables.Remove(workCycle);
                        workCycle.Dispose();
                        LastMonsterSpawned?.Invoke();
                    }
                });
            disposables.Add(workCycle);
        }

        private Transform GetStartPoint(int rawIndex)
        {
            return points[rawIndex % points.Count];
        }

        private void SpawnEnemy(MonsterModel model, Transform startPiont)
        {
            var view = viewsHolder.GetMonsterView(model);
            view.Destroyed += PutBackView;

            var controller = MonsterControllersFactory.CreateController(model, view);
            controller.Start(startPiont);
            MonsterSpawned?.Invoke(controller);
        }

        private void PutBackView(MonsterView view)
        {
            view.Destroyed -= PutBackView;
            viewsHolder.PutBackView(view.Model, view);
        }

        private void StopWork()
        {
            disposables.Dispose();
        }

        public void Dispose()
        {
            StopWork();
        }
    }
}