﻿namespace GameSpace.Monsters
{
    public static class MonsterControllersFactory
    {
        public static MonsterController  CreateController(MonsterModel model, MonsterView view)
        {
            switch (model.Type)
            {
                case MonsterType.EasyWarrior:
                    return new EasyWarriorController(model, view);
                case MonsterType.MediumWarrior:
                    return new EasyWarriorController(model, view);
                case MonsterType.HeavyWarrior:
                    return new EasyWarriorController(model, view);
                default:
                    return new EasyWarriorController(model, view);
            }
        }
    }
}