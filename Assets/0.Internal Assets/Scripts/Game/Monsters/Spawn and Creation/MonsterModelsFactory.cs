﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace GameSpace.Monsters
{
    public static class MonsterModelsFactory
    {
        public static MonsterModel CreateModel(MonsterType type, float spawnDelay, int spawnPointIndex)
        {
            var model = new MonsterModel();
            model.Type = type;

            switch (type)
            {
                case MonsterType.EasyWarrior:
                    model.Hp = 100;
                    model.Armor = 0;
                    model.Damage = 15;
                    model.AttackSpeed = 40;
                    break;
                case MonsterType.MediumWarrior:
                    model.Hp = 150;
                    model.Armor = 0.3f;
                    model.Damage = 20;
                    model.AttackSpeed = 65;
                    break;
                case MonsterType.HeavyWarrior:
                    model.Hp = 200;
                    model.Armor = 0.6f;
                    model.Damage = 30;
                    model.AttackSpeed = 50;
                    break;
                default:
                    model.Hp = 100;
                    model.Armor = 0;
                    model.Damage = 15;
                    model.AttackSpeed = 40;
                    break;
            }

            return model;
        }

        public static MonsterType GetRandomType()
        {
            int max = Enum.GetValues(typeof(MonsterType)).Length;
            return (MonsterType)Random.Range(0, max);
        }
    }
}