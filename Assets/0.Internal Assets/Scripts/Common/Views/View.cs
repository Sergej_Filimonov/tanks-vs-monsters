﻿using System;
using UnityEngine;

namespace Common.Views
{
	public abstract class View : MonoBehaviour, IDisposable
	{
		[SerializeField] protected GameObject objectToHide;
		[SerializeField] protected bool autoInit = false;

        public bool IsActive { get; private set; } = false;
		public bool IsInited { get; private set; } = false;

		protected virtual void Awake()
		{
			if (objectToHide == null) objectToHide = gameObject;
            if (autoInit)
            {
				IsActive = objectToHide.activeSelf;
				if (IsActive)
				{
					ShowAndInit();
				}
			}
		}

		private void ShowAndInit()
        {
			Init();
			IsInited = true;
			OnShown();
        }

		public void Show()
		{
			if (IsActive) return;
			if (objectToHide != null) objectToHide.SetActive(true);
			if (!IsInited)
			{
				Init();
				IsInited = true;
			}
			OnShown();
			IsActive = true;
		}

		protected virtual void Init() { }

		protected virtual void OnShown() {}

        protected virtual void BeforeHide() {}

        public virtual void Hide()
		{
			if (!IsActive) return;
            BeforeHide();
			if (objectToHide != null) objectToHide.SetActive(false);
			IsActive = false;
		}

		public virtual void Dispose() { }

        protected virtual void OnDestroy() => Dispose();
    }
}