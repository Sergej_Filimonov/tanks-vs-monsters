﻿using UnityEngine;

namespace Common.Views
{
	public abstract class TargetView<TTarget> : View
	{
		protected TTarget Target { get; private set; }
        protected bool hasTarget { get; private set; } = false;

        public void SetTarget(TTarget target)
        {
            Target = target;
            hasTarget = true;
        }

        public new void Show()
		{
            if (!hasTarget)
            {
                Debug.LogError("No target! Viewer name:" + gameObject.name);
                return;
            }
			base.Show();
		}
	}
}