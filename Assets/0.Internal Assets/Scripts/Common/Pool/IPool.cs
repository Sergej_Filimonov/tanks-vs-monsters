﻿using System.Collections.Generic;
using UnityEngine;

namespace Common.Pool
{
    public interface IPool<T>
    {
        int Size { get; }

        void PutBack(T item);

        T GetItem();

        T GetItem(Vector3 spawnPosition, Quaternion spawnRotation);

        IEnumerable<T> GetAllItems();

        void Reset();
    }
}