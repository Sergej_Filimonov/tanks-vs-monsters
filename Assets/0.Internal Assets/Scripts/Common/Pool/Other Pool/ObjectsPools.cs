﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Common.Pool
{
    public class ObjectsPools : MonoBehaviour, IDisposable
    {
        public static ObjectsPools Instance;

        private Dictionary<int, Pool> pools = new Dictionary<int, Pool>(100);

        private void Awake()
        {
            Instance = this;
        }

        public bool HasPool(PoolType id) => pools.ContainsKey((int)id);

        public Pool AddPool(PoolType id, bool reparent = true)
        {
            Pool pool;

            if (pools.TryGetValue((int)id, out pool) == false)
            {
                pool = new Pool();

                pools.Add((int)id, pool);

                if (reparent)
                {
                    var poolsGO = GameObject.Find("[POOLS]") ?? new GameObject("[POOLS]");
                    poolsGO.transform.parent = transform;
                    var poolGO = new GameObject("Pool:" + id);
                    poolGO.transform.parent = poolsGO.transform;
                    pool.SetParent(poolGO.transform);
                }
            }
            return pool;
        }

        public MonoBehaviour Spawn(PoolType id, MonoBehaviour prefab, Vector3 position = default(Vector3),
                Quaternion rotation = default(Quaternion),
                Transform parent = null)
        {
            return pools[(int)id].Spawn(prefab, position, rotation, parent);
        }

        public T Spawn<T>(PoolType id, T prefab, Vector3 position = default(Vector3),
            Quaternion rotation = default(Quaternion),
            Transform parent = null) where T : MonoBehaviour
        {
            var val = pools[(int)id].Spawn(prefab, position, rotation, parent);
            return val.GetComponent<T>();
        }

        public void Despawn(PoolType id, MonoBehaviour obj)
        {
            pools[(int)id].Despawn(obj);
        }

        public void Dispose()
        {
            foreach (var poolsValue in pools.Values)
                poolsValue.Dispose();
            pools.Clear();
        }
    }
}