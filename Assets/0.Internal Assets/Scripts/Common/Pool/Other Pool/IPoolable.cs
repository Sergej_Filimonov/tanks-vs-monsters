﻿namespace Common.Pool
{
    public interface IPoolable
    {
        void OnSpawn();
        void OnDespawn();
    }
}