﻿namespace Common.Pool
{
    public enum PoolType
    {
        Entities = 0,
        ProjectileHitEffects = 1,
        Projectiles = 2,
        Items = 3,
        Views = 4
    }
}