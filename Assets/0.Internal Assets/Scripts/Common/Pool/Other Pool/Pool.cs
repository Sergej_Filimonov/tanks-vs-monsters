﻿using UnityEngine;
using System.Collections.Generic;
using System;
using Object = UnityEngine.Object;

namespace Common.Pool
{
    public class Pool : IDisposable
    {
        private Transform parentPool;
        private Dictionary<int, Stack<MonoBehaviour>> cachedObjects = new Dictionary<int, Stack<MonoBehaviour>>(100);
        private Dictionary<int, int> cachedIds = new Dictionary<int, int>(100);

        public void SetParent(Transform parent)
        {
            this.parentPool = parent;
        }

        public MonoBehaviour Spawn(MonoBehaviour prefab, Vector3 position = default(Vector3), Quaternion rotation = default(Quaternion), Transform parent = null)
        {
            MonoBehaviour result = null;
            var key = prefab.GetInstanceID();
            var stack = new Stack<MonoBehaviour>();
            var stacked = cachedObjects.TryGetValue(key, out stack);

            if (stacked)
            {
                if (stack.Count > 0)
                {
                    result = stack.Pop();
                    result.transform.parent = parent;
                    result.transform.rotation = rotation;
                    result.gameObject.SetActive(true);

                    if (parent)
                    {
                        result.transform.localPosition = position;
                    }
                    else
                    {
                        result.transform.position = position;
                    }
                }
            }
            else
            {
                if (!stacked)
                {
                    cachedObjects.Add(key, new Stack<MonoBehaviour>());
                }
            }

            if (result == null)
            {
                result = Populate(prefab, position, rotation, parent);
                cachedIds.Add(result.GetInstanceID(), key);
            }

            var poolable = result.GetComponent<IPoolable>();

            if (poolable != null)
            {
                poolable.OnSpawn();
            }

            return result;
        }

        public void Despawn(MonoBehaviour go)
        {
            go.gameObject.SetActive(false);
            cachedObjects[cachedIds[go.GetInstanceID()]].Push(go);

            var poolable = go.GetComponent<IPoolable>();

            if (poolable != null)
            {
                poolable.OnDespawn();
            }

            if (parentPool != null)
            {
                go.transform.parent = parentPool;
            }
        }

        MonoBehaviour Populate(MonoBehaviour prefab, Vector3 position = default(Vector3), Quaternion rotation = default(Quaternion), Transform parent = null)
        {
            var go = Object.Instantiate(prefab, position, rotation, parent);
            if (parent == null)
            {
                go.transform.position = position;
            }
            else
            {
                go.transform.localPosition = position;
            }

            return go;
        }

        public void Dispose()
        {
            parentPool = null;
            cachedObjects.Clear();
            cachedIds.Clear();
        }
    }
}