﻿using System;
using UnityEngine;

namespace Common.Pool
{
    public sealed class NonExpandableItemPool<T> : ExpandableItemPool<T> 
        where T : Component, IPoolItem
    {
        private readonly int maxItemCount;

        public NonExpandableItemPool(CreatorDelegate itemCreator, Action<T> itemInitializer, int preCreation = 0, int maxItemCount = 0)
            : base(itemCreator, itemInitializer, CheckCount(preCreation, maxItemCount))
        {
            this.maxItemCount = maxItemCount;
        }

        protected override void Create()
        {
            if (maxItemCount < InitializedItemsCount) return;

            base.Create();
        }

        private static int CheckCount(int preCreation, int maxItem)
        {
            return preCreation > maxItem ? maxItem : preCreation;
        }
    }
}
