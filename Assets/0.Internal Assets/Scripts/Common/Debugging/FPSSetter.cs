using UnityEngine;

namespace UnityLibrary.Debugging
{
    public sealed class FPSSetter : MonoBehaviour
    {
        [SerializeField] private bool useDefaultFrameRate = false;
        [SerializeField] private int targetFrameRate = 60;
        [SerializeField] private VSyncType VSync = VSyncType.No;

        private void Awake()
        {
            if (useDefaultFrameRate)
            {
                QualitySettings.vSyncCount = 0;
                Application.targetFrameRate = -1;
            }
            else
            {
                QualitySettings.vSyncCount = (int)VSync;
                Application.targetFrameRate = targetFrameRate;
            }
        }

        private void OnValidate()
        {
            if (useDefaultFrameRate)
            {
                VSync = VSyncType.No;
                targetFrameRate = -1;
            }
        }

        public enum VSyncType
        {
            No = 0,
            EveryVBlank = 1,
            EverySecondVBlank = 2
        }
    }
}