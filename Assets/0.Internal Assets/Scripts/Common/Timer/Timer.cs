﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Common.Timer
{
	public sealed class Timer : MonoBehaviour
	{
		private static Timer instance;

		private Dictionary<TimerBadge, Coroutine> coroutines;
		private Dictionary<TimerBadge, Action> updateActions;
        private Dictionary<TimerBadge, Action> fixedUpdateActions;

        private List<Action> updateActionsList;
        private List<Action> fixedUpdateActionsList;

        public static bool Exists => instance != null;

		private void Awake()
		{
			coroutines = new Dictionary<TimerBadge, Coroutine>();
			updateActions = new Dictionary<TimerBadge, Action>();
            fixedUpdateActions = new Dictionary<TimerBadge, Action>();
			updateActionsList = new List<Action>();
            fixedUpdateActionsList = new List<Action>();
			
			instance = this;
        }

        public static TimerBadge Set(float seconds, Action callback, bool isRealTime = false)
		{
			var timerBadge = new TimerBadge(instance.coroutines.Count, DateTime.Now, seconds);
            if (isRealTime)
            {
                instance.coroutines.Add(timerBadge, instance.StartCoroutine(RunRealTimer(timerBadge, seconds, callback)));
            }
            else
            {
                instance.coroutines.Add(timerBadge, instance.StartCoroutine(RunTimer(timerBadge, seconds, callback)));
            }
            return timerBadge;
		}

        public static TimerBadge RunInFixedUpdate(Action action)
        {
            var timerBadge = new TimerBadge(instance.fixedUpdateActions.Count, DateTime.Now);
            instance.fixedUpdateActions.Add(timerBadge, action);
            instance.fixedUpdateActionsList.Add(action);
            return timerBadge;
        }

        public static TimerBadge RunInUpdate(Action action)
        {
            var timerBadge = new TimerBadge(instance.updateActions.Count, DateTime.Now);
            instance.updateActions.Add(timerBadge, action);
            instance.updateActionsList.Add(action);
            return timerBadge;
        }

        /// <summary>
        /// Execute action every frame for seconds
        /// </summary>
        /// <param name="action"></param>
        /// <param name="timer">Time for executing action</param>
        /// <returns></returns>
        public static TimerBadge RunInUpdate(Action action, float timer)
        {
            var timerBadge = RunInUpdate(action);
            Set(timer, () => { StopUpdate(timerBadge); });
            return timerBadge;
        }

        /// <summary>
        /// Execute action every frame for seconds
        /// </summary>
        /// <param name="action"></param>
        /// <param name="timer">Time for executing action</param>
        /// <param name="endCallback">Executed when timer is ended</param>
        /// <returns></returns>
        public static TimerBadge RunInUpdate(Action action, float timer, Action endCallback)
        {
            var timerBadge = RunInUpdate(action);
            Set(timer, () =>
            {
                StopUpdate(timerBadge);
                endCallback.Invoke();
            });
            return timerBadge;
        }

        /// <summary>
        /// Repeat action per time interval
        /// </summary>
        /// <param name="action"></param>
        /// <param name="interval"></param>
        /// <returns></returns>
        public static TimerBadge Repeat(Action action, float interval)
        {
            var timerBadge = new TimerBadge(instance.coroutines.Count, DateTime.Now);
            instance.coroutines.Add(timerBadge, instance.StartCoroutine(RunAction(interval, action)));
            return timerBadge;
        }

        /// <summary>
        /// Repeat action per time interval for seconds
        /// </summary>
        /// <param name="action"></param>
        /// <param name="interval"></param>
        /// <param name="timer"></param>
        /// <returns></returns>
        public static TimerBadge Repeat(Action action, float interval, float timer)
        {
            var timerBadge = Repeat(action, interval);
            Set(timer, () => { Stop(timerBadge); });
            return timerBadge;
        }

        public static void Stop(TimerBadge timerBadge)
        {
            if (!instance.coroutines.ContainsKey(timerBadge)) return;
            instance.StopCoroutine(instance.coroutines[timerBadge]);
            instance.coroutines.Remove(timerBadge);
            timerBadge.Expire();
        }

        public static void StopUpdate(TimerBadge timerBadge)
        {
            if (!instance.updateActions.ContainsKey(timerBadge)) return;
            instance.updateActionsList.Remove(instance.updateActions[timerBadge]);
            instance.updateActions.Remove(timerBadge);
            timerBadge.Expire();
        }

        public static void StopFixedUpdate(TimerBadge timerBadge)
        {
            if (!instance.fixedUpdateActions.ContainsKey(timerBadge)) return;
            instance.fixedUpdateActionsList.Remove(instance.fixedUpdateActions[timerBadge]);
            instance.fixedUpdateActions.Remove(timerBadge);
            timerBadge.Expire();
        }

        public static void StopAll()
		{
			StopAllRegular();
			StopAllUpdate();
            StopAllFixedUpdate();
        }
		
		public static void StopAllRegular()
		{
			instance.StopAllCoroutines();
			instance.coroutines.Clear();
		}

        public static void StopAllUpdate()
        {
            instance.updateActions.Clear();
            instance.updateActionsList.Clear();
        }
        
        public static void StopAllFixedUpdate()
        {
            instance.fixedUpdateActions.Clear();
            instance.fixedUpdateActionsList.Clear();
        }

        private static IEnumerator RunTimer(TimerBadge timerBadge, float seconds, Action callback)
		{
			yield return new WaitForSeconds(seconds);
			instance.coroutines.Remove(timerBadge);
			callback?.Invoke();
		}

        private static IEnumerator RunRealTimer(TimerBadge timerBadge, float seconds, Action callback)
        {
            yield return new WaitForSecondsRealtime(seconds);
            instance.coroutines.Remove(timerBadge);
            callback?.Invoke();
        }

        private static IEnumerator RunAction(float interval, Action action)
        {
            WaitForSeconds wait = new WaitForSeconds(interval);
            while (true)
            {
                action.Invoke();
                yield return wait;
            }
        }

		private void Update() => updateActionsList.ToList().ForEach(a => a?.Invoke());

        private void FixedUpdate() => fixedUpdateActionsList.ToList().ForEach(a => a?.Invoke());

        private void OnDestroy() => StopAll();
	}
}