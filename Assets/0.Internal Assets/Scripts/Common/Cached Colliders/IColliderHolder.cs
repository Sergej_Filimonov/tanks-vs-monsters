﻿using UnityEngine;

namespace Common.CachedCollidersSystem
{
    public interface IColliderHolder<TComponent>
    {
        Collider Collider { get; }
        TComponent Component { get; }

        void CachedCollideWithComponent(IColliderHolder<TComponent> holder);
    }
}