﻿using System.Collections.Generic;
using UnityEngine;

namespace Common.CachedCollidersSystem
{
    public static class CachedColliders
    {
        private static Dictionary<Collider, object> cach = new Dictionary<Collider, object>();

        public static void Add<T>(IColliderHolder<T> holder)
        {
            cach.Add(holder.Collider, holder.Component);
        }

        public static void Remove<T>(IColliderHolder<T> holder)
        {
            if (cach.ContainsKey(holder.Collider))
            {
                cach.Remove(holder.Collider);
            }
        }

        public static void Clear() => cach.Clear();

        public static bool Get<T>(Collider collider, out T component)
        {
            if (cach.ContainsKey(collider))
            {
                component = (T)cach[collider];
                return true;
            }
            else
            {
                component = default(T);
                return false;
            }
        }
    }
}