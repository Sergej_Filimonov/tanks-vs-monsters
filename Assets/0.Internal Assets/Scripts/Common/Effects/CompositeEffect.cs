﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Common.Effects
{
    public sealed class CompositeEffect : Effect
    {
        [SerializeField] private List<Effect> effects = new List<Effect>();
        private float duration;

        public override float Duration
        {
            get { return duration; }
        }

        private void Awake()
        {
            duration = effects.Select(effect => effect.Duration).Max();
        }

        public override void Play()
        {
            for (int i = 0; i < effects.Count; i++)
            {
                effects[i].Play();
            }
        }

        public override void Stop()
        {
            for (int i = 0; i < effects.Count; i++)
            {
                effects[i].Stop();
            }
        }
    }
}
