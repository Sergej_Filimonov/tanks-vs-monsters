﻿using UnityEngine;

namespace Common.Effects
{
    public abstract class Effect : MonoBehaviour
    {
        public abstract float Duration { get; }

        public abstract void Play();

        public abstract void Stop();
    }
}
