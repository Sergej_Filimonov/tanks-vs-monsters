﻿namespace Common.Effects
{
    public interface IEffect<T>
    {
        float Duration { get; }

        void Play();

        void Stop();
    }
}
