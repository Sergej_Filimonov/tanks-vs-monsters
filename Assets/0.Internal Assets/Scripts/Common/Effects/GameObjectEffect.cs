﻿using UnityEngine;

namespace Common.Effects
{
    public sealed class GameObjectEffect : Effect
    {
        [SerializeField] private GameObject effect = null;
        [SerializeField] private float duration = 1.0f;

        public override float Duration
        {
            get { return duration; }
        }

        public override void Play()
        {
            effect.SetActive(true);
        }

        public override void Stop()
        {
            effect.SetActive(false);
        }
    }
}
