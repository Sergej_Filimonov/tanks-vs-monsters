﻿namespace Common.Architecture
{
    public abstract class SceneController<SceneDataType>
    {
        #region PublicProperty
        public SceneDataType SceneData => sceneData;
        #endregion

        #region PrivetFields
        protected SceneDataType sceneData;
        #endregion

        #region Initialization
        public void SetupData(SceneDataType gameSceneData)
        {
            sceneData = gameSceneData;
        }
        #endregion

        #region IController
        public abstract void Start();

        public abstract void Stop();
        #endregion
    }
}