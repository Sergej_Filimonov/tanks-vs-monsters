﻿using UnityEngine;

namespace Common.SceneManagement
{
    public interface ILoadingScreenHolder
    {
        void ShowLoadingScreen(AsyncOperation asyncOperation);
        void HideLoadingScreen();
    }
}