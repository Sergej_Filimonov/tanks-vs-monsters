﻿using UnityEngine;
using UnityEngine.UI;

namespace Common.SceneManagement
{
    [RequireComponent(typeof(Slider))]
    public class SliderProgressDisplayer : MonoBehaviour, IProgressDiplayer
    {
        private Slider slider;

        private void Awake()
        {
            slider = GetComponent<Slider>();
        }

        public float CurrentProgress => slider.value;

        public void SetProgress(float value)
        {
            slider.value = Mathf.Clamp01(value);
        }
    }
}