using System;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

namespace Common.SceneManagement
{
    public static class ScenesLoader
	{
		public static event Action<Scene, LoadSceneMode> SceneLoaded;
		public static event Action<Scene> SceneUnloaded;

		private static readonly Dictionary<string, bool> SetActives = new Dictionary<string, bool>();
        private static bool needSubscribe = true;

		/// <summary>
		///     Gets an active scene name
		/// </summary>
		/// <returns>Returns the name of the active scene</returns>
		public static string CurrentSceneName => SceneManager.GetActiveScene().name;

        #region LoadScene

        /// <summary>
        ///     Loads a given scene
        /// </summary>
        /// <param name="scene">A scene to load</param>
        /// <param name="setActive">Whether to set this scene active after loading</param>
        /// <param name="loadAsync">Whether to load the scene asynchronously</param>
        /// <param name="additive">Can be either single or additional</param>
        public static void LoadScene(ApplicationScene scene, bool setActive = true, bool loadAsync = true, bool additive = true, ILoadingScreenHolder loadingScreenHolder = null) =>
            LoadScene(scene.ToString(), setActive, loadAsync, additive, loadingScreenHolder);

		/// <summary>
		///     Loads a given scene
		/// </summary>
		/// <param name="sceneIndex">A scene to load</param>
		/// <param name="setActive">Whether to set this scene active after loading</param>
		/// <param name="loadAsync">Whether to load the scene asynchronously</param>
		/// <param name="additive">Can be either single or additional</param>
		public static void LoadScene(int sceneIndex, bool setActive = true, bool loadAsync = true, bool additive = true, ILoadingScreenHolder loadingScreenHolder = null) =>
			LoadScene(SceneManager.GetSceneByBuildIndex(sceneIndex).name, setActive, loadAsync, additive, loadingScreenHolder);

		/// <summary>
		///     Loads a given scene
		/// </summary>
		/// <param name="sceneName">A scene to load</param>
		/// <param name="setActive">Whether to set this scene active after loading</param>
		/// <param name="loadAsync">Whether to load the scene asynchronously</param>
		/// <param name="additive">Can be either single or additional</param>
		public static void LoadScene(string sceneName, bool setActive = true, bool loadAsync = true, bool additive = true, ILoadingScreenHolder loadingScreenHolder = null)
		{
			SetActives.Add(sceneName, setActive);
            if (needSubscribe)
            {
                SceneManager.sceneLoaded += OnSceneLoaded;
                needSubscribe = false;
            } 

			var mode = additive ? LoadSceneMode.Additive : LoadSceneMode.Single;

            if (!loadAsync)
            {
                SceneManager.LoadScene(sceneName, mode);
            }
            else
            {
                var asyncOperation = SceneManager.LoadSceneAsync(sceneName, mode);

                if (loadingScreenHolder != null)
                    loadingScreenHolder.ShowLoadingScreen(asyncOperation);
            }
		}

        private static void OnSceneLoaded(Scene scene, LoadSceneMode mode)
		{
			var setActive = true;
			if (SetActives.ContainsKey(scene.name))
			{
				setActive = SetActives[scene.name];
				SetActives.Remove(scene.name);
			}
			if (setActive) SceneManager.SetActiveScene(scene);
			SetActives.Remove(scene.name);
			SceneLoaded?.Invoke(scene, mode);
		}

		#endregion

		#region SetActiveScene

		public static void SetActiveScene(ApplicationScene scene) => SetActiveScene(scene.ToString());

		public static void SetActiveScene(int sceneIndex) =>
			SetActiveScene(SceneManager.GetSceneByBuildIndex(sceneIndex).name);

		public static void SetActiveScene(string sceneName) =>
			SceneManager.SetActiveScene(SceneManager.GetSceneByName(sceneName));

		#endregion

		#region UnloadScene

		/// <summary>
		///     Unloads a given scene
		/// </summary>
		/// <param name="scene">A scene to unload</param>
		public static void UnloadScene(ApplicationScene scene) => UnloadScene(scene.ToString());

		/// <summary>
		///     Unloads a given scene
		/// </summary>
		/// <param name="sceneIndex">A scene to unload</param>
		public static void UnloadScene(int sceneIndex) =>
			UnloadScene(SceneManager.GetSceneByBuildIndex(sceneIndex).name);

		/// <summary>
		///     Unloads a given scene
		/// </summary>
		/// <param name="sceneName">A scene to unload</param>
		public static void UnloadScene(string sceneName)
		{
			SceneManager.sceneUnloaded += OnSceneUnloaded;
			SceneManager.UnloadSceneAsync(sceneName);
		}

		private static void OnSceneUnloaded(Scene scene)
		{
			SceneManager.sceneUnloaded -= OnSceneUnloaded;
			SceneUnloaded?.Invoke(scene);
		}

		#endregion
	}
}