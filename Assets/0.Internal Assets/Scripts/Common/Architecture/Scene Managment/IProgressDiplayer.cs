﻿namespace Common.SceneManagement
{
    public interface IProgressDiplayer
    {
        float CurrentProgress { get; }

        void SetProgress(float value);
    }
}