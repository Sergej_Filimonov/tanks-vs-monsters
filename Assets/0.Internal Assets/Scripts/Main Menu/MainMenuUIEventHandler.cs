﻿using UnityEngine;
using Common.Architecture.Templates;

namespace MainMenuSpace.Handlers
{
    public class MainMenuUIEventHandler : MonoBehaviour
    {
        public void PlayWithMiddleTank()
        {
            ApplicationManager.Instance.RunGame(0);
        }

        public void PlayWithHeavyTank()
        {
            ApplicationManager.Instance.RunGame(1);
        }
    }
}