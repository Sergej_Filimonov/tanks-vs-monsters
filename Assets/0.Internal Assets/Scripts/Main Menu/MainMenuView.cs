﻿using Common.Views;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace MainMenuSpace.Views
{
    public class MainMenuView : View
    {
        [SerializeField] private Button middleTank = null;
        [SerializeField] private Button heavyTank = null;

        public event Action MiddleTankButtonClicked;
        public event Action HeavyTankButtonClicked;

        protected override void Init()
        {
            middleTank.onClick.RemoveAllListeners();
            middleTank.onClick.AddListener(() => MiddleTankButtonClicked?.Invoke());

            heavyTank.onClick.RemoveAllListeners();
            heavyTank.onClick.AddListener(() => HeavyTankButtonClicked?.Invoke());
        }
    }
}