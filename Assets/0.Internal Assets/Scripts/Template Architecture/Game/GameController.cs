﻿using GameSpace.Tanks;
using System;
using UniRx;
using GameSpace.Monsters;
using UnityEngine;

namespace Common.Architecture.Templates
{
    public class GameController : SceneController<GameSceneData>
    {
        public TankController TankController => tankController;

        private GameData gameData;
        private GameFieldSceneData gameFieldSceneData;
        private TankController tankController;
        private MonsterViewsHolder monsterViewsHolder;
        private MonstersSpawner monstersSpawner;
        private MonstersPopulation monstersPopulation;
        private CompositeDisposable disposables;

        public override void Start()
        {
            gameData = ApplicationManager.Instance.Game.Data;

            disposables = new CompositeDisposable();

            monsterViewsHolder = new MonsterViewsHolder(ApplicationManager.Instance.MonstersView);

            if (ApplicationManager.Instance.GameField.IsLoaded)
            {
                OnDataLoaded();
            }
            else
            {
                ApplicationManager.Instance.GameField.Loaded += OnDataLoaded;
            }
        }

        public override void Stop()
        {
            disposables.Dispose();
        }

        private void OnDataLoaded()
        {
            gameFieldSceneData = ApplicationManager.Instance.GameField.Controller.SceneData;

            monstersSpawner = new MonstersSpawner(gameFieldSceneData.MonsterSpawnPoint, monsterViewsHolder);
            disposables.Add(monstersSpawner);

            monstersPopulation = new MonstersPopulation(monstersSpawner);
            disposables.Add(monstersPopulation);
            monstersPopulation.SartWork();
            monstersPopulation.MonsterDamaged += (c, d) => sceneData.DamageViewSystem.Show(c.View.transform.position, d, Color.white);

            sceneData.Input.Init();

            SetupTank();

            gameFieldSceneData.CameraMover.SetTargets(tankController.View.Frame, sceneData.Input.Pointer);
            disposables.Add(gameFieldSceneData.CameraMover);
        }

        private void SetupTank()
        {
            tankController = gameData.TankAsset.CreateController(gameFieldSceneData.TankPosition);
            disposables.Add(tankController);

            sceneData.Input.MoveInput += tankController.Move;
            sceneData.Input.PointerInput += tankController.LookAtTarget;
            sceneData.Input.PointerDown += tankController.Shot;
            sceneData.Input.SwitchWeapon += tankController.SwitchWeapon;

            tankController.Damaged += d => sceneData.DamageViewSystem.Show(tankController.View.Frame.position, d, Color.red);
            tankController.Dead += tc => GameOver();
        }

        private void GameOver()
        {
            sceneData.Input.MoveInput -= tankController.Move;
            sceneData.Input.PointerInput -= tankController.LookAtTarget;
            sceneData.Input.PointerDown -= tankController.Shot;
            sceneData.Input.SwitchWeapon -= tankController.SwitchWeapon;
            tankController.Dispose();
            sceneData.GameOverObject.SetActive(true);
            Observable.Timer(TimeSpan.FromSeconds(3.0f)).Subscribe(_ => ApplicationManager.Instance.BackToMainMenu());
        }
    }
}