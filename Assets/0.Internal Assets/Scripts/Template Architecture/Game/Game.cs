﻿namespace Common.Architecture.Templates
{
    public class Game : SceneImplementer<GameController, GameSceneData, GameData>
    {
        public Game(GameData gameData) : base(gameData)
        {

        }

        protected override void SetScene()
        {
            Scene = SceneManagement.ApplicationScene.Game;
        }
    }
}