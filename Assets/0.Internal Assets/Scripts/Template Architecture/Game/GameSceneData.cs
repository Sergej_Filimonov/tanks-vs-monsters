﻿using GameSpace.InputSystem;
using GameSpace.Views;
using UnityEngine;

namespace Common.Architecture.Templates
{
    public class GameSceneData : MonoBehaviour
    {
        [SerializeField] private PlayerInput input = null;
        [SerializeField] private DamageViewSystem damageViewSystem = null;
        [SerializeField] private GameObject gameOverObject = null;

        public PlayerInput Input { get => input;}
        public DamageViewSystem DamageViewSystem { get => damageViewSystem; }
        public GameObject GameOverObject { get => gameOverObject; }
    }
}