﻿using GameSpace.Tanks;

namespace Common.Architecture.Templates
{
    public class GameData
    {
        public TankAsset TankAsset { get; private set; }

        public GameData(TankAsset tankAsset)
        {
            TankAsset = tankAsset;
        }
    }
}