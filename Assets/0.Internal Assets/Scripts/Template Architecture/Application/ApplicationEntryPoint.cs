﻿using UnityEngine;

namespace Common.Architecture.Templates
{
    public class ApplicationEntryPoint : MonoBehaviour
    {
        private LoadingScreen loadingScreen;
        private UICamera uICamera;

        private void Awake()
        {
            loadingScreen = new LoadingScreen();
            loadingScreen.Load(false, false);
            uICamera = new UICamera();
            uICamera.Loaded += OnFirstScenesLoaded;
            uICamera.Load(false, false);
        }

        private void OnFirstScenesLoaded()
        {
            uICamera.Loaded -= OnFirstScenesLoaded;
            SceneManagement.ScenesLoader.SceneLoaded += OnApplicationLoaded;
            SceneManagement.ScenesLoader.LoadScene(SceneManagement.ApplicationScene.Application, true, true, true, loadingScreen);
        }

        private void OnApplicationLoaded(UnityEngine.SceneManagement.Scene scene, UnityEngine.SceneManagement.LoadSceneMode mode)
        {
            if (scene.name == SceneManagement.ApplicationScene.Application.ToString())
            {
                SceneManagement.ScenesLoader.SceneLoaded -= OnApplicationLoaded;
                ApplicationManager.Instance.SetData(loadingScreen, uICamera);
                SceneManagement.ScenesLoader.UnloadScene(SceneManagement.ApplicationScene.EntryPoint);
            }
        }
    }
}