﻿using GameSpace.Tanks;
using GameSpace.Weapons;
using UnityEngine;
using UnityEngine.EventSystems;
using GameSpace.Monsters;
using System.Collections.Generic;

namespace Common.Architecture.Templates
{
    public class ApplicationManager : MonoBehaviour
    {
        [Header("Game data")]
        [SerializeField] private MonsterTypeViewPair[] monstersViews = null;
        [SerializeField] private TankAsset[] tanks = null;

        [Space]
        [Header("System")]
        [SerializeField] private AudioListener audioListener = null;
        [SerializeField] private EventSystem eventSystem = null;

        public static ApplicationManager Instance { get; private set; }

        public AudioListener AudioListener => audioListener;
        public EventSystem EventSystem => eventSystem;
        public LoadingScreen LoadingScreen { get; private set; }
        public UICamera UICamera { get; private set; }
        public AudioSystem AudioSystem { get; private set; }
        public MainMenu MainMenu { get; private set; }
        public Game Game { get; private set; }
        public GameField GameField { get; private set; }

        public Dictionary<MonsterType, MonsterView> MonstersView { get; private set; }

        private void Awake()
        {
            Instance = this;

            MonstersView = new Dictionary<MonsterType, MonsterView>(monstersViews.Length);
            foreach (var pair in monstersViews)
            {
                MonstersView.Add(pair.Type, pair.View);
            }
        }

        public void SetData(LoadingScreen loadingScreen, UICamera uICamera)
        {
            LoadingScreen = loadingScreen;
            UICamera = uICamera;
            LoadOtherScenes();
        }

        private void LoadOtherScenes()
        {
            AudioSystem = new AudioSystem();
            AudioSystem.Load(false, true, LoadingScreen);
            MainMenu = new MainMenu();
            MainMenu.Load(loadingScreenHolder: LoadingScreen);
        }

        public void RunGame(int tankIndex)
        {
            MainMenu.Unload();
            MainMenu = null;

            var gameData = new GameData(tanks[tankIndex]);
            Game = new Game(gameData);
            GameField = new GameField();

            Game.Load();
            GameField.Load();
        }

        public void BackToMainMenu()
        {
            Game.Unload();
            Game = null;
            GameField.Unload();
            GameField = null;

            MainMenu = new MainMenu();
            MainMenu.Load();
        }
    }
}