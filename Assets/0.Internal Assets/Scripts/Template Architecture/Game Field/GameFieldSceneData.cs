﻿using GameSpace;
using System.Collections.Generic;
using UnityEngine;

namespace Common.Architecture.Templates
{
    public class GameFieldSceneData : MonoBehaviour
    {
        [SerializeField] private Transform tankPosition = null;
        [SerializeField] private List<Transform> monsterSpawnPoint = null;
        [SerializeField] private CameraMover cameraMover = null;

        public Transform TankPosition { get => tankPosition; }
        public List<Transform> MonsterSpawnPoint { get => monsterSpawnPoint; }
        public CameraMover CameraMover { get => cameraMover; }
    }
}