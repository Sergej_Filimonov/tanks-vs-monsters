﻿namespace Common.Architecture.Templates
{
    public class GameField : SceneImplementer<GameFieldController, GameFieldSceneData, GameFieldData>
    {
        protected override void SetScene()
        {
            Scene = SceneManagement.ApplicationScene.GameField;
        }
    }
}