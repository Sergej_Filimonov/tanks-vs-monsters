﻿using Common.SceneManagement;
using UnityEngine;

namespace Common.Architecture.Templates
{
    public class LoadingScreen : SceneImplementer<LoadingSceenContreller, LoadingScreenSceneData, EmptyData>, ILoadingScreenHolder
    {
        protected override void SetScene()
        {
            Scene = ApplicationScene.LoadingScreen;
        }

        public void ShowLoadingScreen(AsyncOperation asyncOperation)
        {
            Controller.SceneData.LoadingScreen.Show(asyncOperation);
        }

        public void HideLoadingScreen()
        {
            Controller.SceneData.LoadingScreen.Hide();
        }
    }
}