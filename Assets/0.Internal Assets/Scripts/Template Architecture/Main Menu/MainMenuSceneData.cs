﻿using UnityEngine;
using MainMenuSpace.Views;
using MainMenuSpace.Handlers;

namespace Common.Architecture.Templates
{
    public class MainMenuSceneData : MonoBehaviour
    {
        [SerializeField] private MainMenuView mainMenuView = null;
        [SerializeField] private MainMenuUIEventHandler uiHandler = null;

        public MainMenuView MainMenuView { get => mainMenuView;}
        public MainMenuUIEventHandler UiHandler { get => uiHandler;}
    }
}