﻿namespace Common.Architecture.Templates
{
    public class MainMenuController : SceneController<MainMenuSceneData>
    {
        public override void Start()
        {
            sceneData.MainMenuView.MiddleTankButtonClicked += sceneData.UiHandler.PlayWithMiddleTank;
            sceneData.MainMenuView.HeavyTankButtonClicked += sceneData.UiHandler.PlayWithHeavyTank;
        }

        public override void Stop()
        {
            sceneData.MainMenuView.MiddleTankButtonClicked -= sceneData.UiHandler.PlayWithMiddleTank;
            sceneData.MainMenuView.HeavyTankButtonClicked -= sceneData.UiHandler.PlayWithHeavyTank;
        }
    }
}