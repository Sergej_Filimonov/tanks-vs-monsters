﻿using UnityEngine;

namespace Common.Architecture.Templates
{
    public class UICameraSceneData : MonoBehaviour
    {
        [SerializeField] private Camera uiCamera = null;

        public Camera UICamera => uiCamera;
    }
}