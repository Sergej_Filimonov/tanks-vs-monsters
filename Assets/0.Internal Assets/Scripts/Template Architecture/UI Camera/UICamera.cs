﻿using UnityEngine;

namespace Common.Architecture.Templates
{
    public class UICamera : SceneImplementer<UICameraController, UICameraSceneData, EmptyData>
    {
        public Camera Camera => Controller.SceneData.UICamera;

        protected override void SetScene()
        {
            Scene = SceneManagement.ApplicationScene.UICamera;
        }
    }
}